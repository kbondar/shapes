/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

/**
 *
 * @author Kate
 */
public class Rectangle extends Shape{
    private double width;
    private double height;

    public Rectangle() {
        super();
        this.height = 0;
        this.width = 0;
    }
    public Rectangle(double x, double y, double height, double width) {
        super(x, y);
        this.height = height;
        this.width = width;
        
    }
    
    @Override
    public void draw() {
    
    }

    @Override
    public void scale(double k) {
        
        this.height *= k;
        this.width *= k;
    }

    @Override
    public double calcSquare() {
        return width * height;
    
    }

    void setWidth(int width) {
        this.width =  width;
    }
    double getWidth() {
        return width;
    }
    void setHeight(int height) {
        this.height =  height;
    }
    double getHeight() {
        return height;
    }
    
    @Override
    public String toString() {
        String text = "";
        text += "Rectangle: \n" 
            + "The coordinate of the rectangle: " 
            + "(" + this.x + ", " + this.y + ").\n"
            + "height = " + getHeight()
            + "\nwidth = " + getWidth()
            + "\n";
        text += "Square = " +  calcSquare();
        text += "\nColor: " + getColor();
        
        return text;
    }
}
