/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

import java.awt.Color;

/**
 *
 * @author Kate
 */
public interface InfoShapes {
    void draw();
    void moveTo( double x, double y );
    void moveBy( double dx, double dy );
    /*
     *
     * @param k scaling factor/масштабный коэффициент
     */
    void scale(double k);
   // void squareShapes(int x, int y);
    double calcSquare();
    //void erase(); 
    Color getColor();
    void setColor(Color color);
    
}
