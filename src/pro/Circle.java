/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

/**
 *
 * @author Kate
 */
public class Circle extends Shape{
    private double r;
    
    public Circle() {
        super();
        this.r = 0.0;
    }
    public Circle(int x, int y, int r) {
        super(x, y);
        this.r = r;
    }
    @Override
    public void draw() {
        for(int i = 0; i < 180; ++i) {
            double root = Math.sqrt(Math.pow(r, 2.0) - Math.pow(i - x, 2.0));
            double yy1 = y + root;
            double yy2 = y - root;
        }
    
    }

    @Override
    public void scale(double k) {
      //  super.scale(x, y, k);
        this.r *= k;
    }

    @Override
    public double calcSquare() {
        return Math.PI * Math.pow(r, 2.0);
    }
   // void move(double r) {
      /*  this.x = x;
        this.y = y;*/
   /*     this.r = r;
        
    }*/
    
    void setR(double r) {
        this.r = r;
    }
    double getR() {
        return r;
    }
    
    @Override
    public String toString() {
        String text = "";
        text += "Circle: \n" 
            + "Coordinate of the circle center: " 
            + "(" + this.x + ", " + this.y + ")."
            + "\nRadius = " + getR()
            + "\n";
        text += "Square = " +  calcSquare();
        text += "\nColor: " + getColor();
        
        return text;
    }
}
