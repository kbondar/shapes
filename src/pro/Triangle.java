/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

/**
 *
 * @author Kate
 */
public class Triangle extends Shape{
    private double x1;
    private double y1;
    private double z;
    private double z1;

    public Triangle() {
        super();
        this.x1 = 0;
        this.y1 = 0;
        this.z = 0;
        this.z1 = 0;
    }
    public Triangle(double x, double x1, double y, double y1, double z, double z1) {
        super(x, y);
        this.x1 = x1;
        this.y1 = y1;
        this.z = z;
        this.z1 = z1;
    }
    @Override
    public void draw() {
    
    }

    /**
     *
     * @param k коэффициент k
     */
    @Override
    public void scale(double k) {
        super.scale(k);
        this.x1 *= k;
        this.y1 *= k;
        this.z *= k;
        this.z1 *= k;            
    }

    @Override
    public double calcSquare() {
        return ((x-z)*(y1-z1) - (y-z)*(x1-z1))/2.0;
    }
    
    /**
     *
     * @param dx coefficient of movements
     * @param dy coefficient of movements
     */
    @Override
    public void moveBy(double dx, double dy) {
        //super.moveBy(dx, dy);
        this.x += dx;
        this.y += dx;
        this.x1 += dy;
        this.y1 += dy;
        this.z += dx;
        this.z1 += dy;
    }

    /**
     *
     * @param x offset
     * @param y
     */
    @Override
    public void moveTo(double x, double y) {
       // super.moveTo(x, y);
        double xdx = x - this.x;
        double ydy = y - this.y;
       /* this.x += xdx;
        this.y += xdx;
        this.z += xdx;
        this.z1 += ydy;
        this.x1 += ydy;
        this.y1 += ydy;*/
        moveBy(xdx, ydy);
    }
    @Override
    public String toString() {
        String text = "";
        text += "Triangle: \n" 
            + "The coordinates of the triangle: " 
            + "(" + this.x + ", " + this.x1 + "); "
            + "(" + this.y + ", " + this.y1 + "); "
            + "(" + this.z + ", " + this.z1 + ").\n";
        text += "Square = " +  calcSquare();
        text += "\nColor: " + getColor();
        
        return text;
    }
    
}
