/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

import java.awt.Color;

/**
 *
 * @author Kate
 */
public class Figuresdemo {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setR(5.0);
       // System.out.println("Square circle = " + circle.calcSquare());
        System.out.println(circle);
        
        Rectangle rectangle = new Rectangle(0, 0, 4, 9);
      //  System.out.println("Square rectangle = " + rectangle.calcSquare());
        System.out.println(rectangle);
        
        Triangle triangle = new Triangle(0, 0, 2, 2, 0, 4);
        triangle.setColor(new Color(0,255,0));
       // triangle.colorShape = new Color(0,255,0);
      //  System.out.println("Square triangle = " +  triangle.calcSquare());
        System.out.println(triangle);
        
    }
    
}
