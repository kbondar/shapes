/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro;

import java.awt.Color;

/**
 *
 * @author Kate
 */
public abstract class Shape implements InfoShapes{
    public double x;
    public double y;
    protected Color colorShape = new Color(255, 0, 0);

    
    public Shape() {
        this.x = 0.0;
        this.y = 0.0;
    }
    public Shape(double x, double y) {
        this.x = x;
        this.y = y;
        //, double dx, double dy
        /*this.dx = dx;
        this.dy = dy;*/
    }
    
    @Override
    public void moveTo(double x, double y) {
        this.x = x;
        this.y = y;
    }
    @Override
    public void moveBy(double dx, double dy) {
        this.x += dx;
        this.y += dy;
    }
    @Override
    public void scale(double k) {
        this.x *= k;
        this.y *= k;
    }
    @Override
    public void setColor(Color color) {
        this.colorShape = color;
    }
    @Override
    public Color getColor() {
        return colorShape;
        
    }
    /*abstract void draw();
    void move(int x, int y) {
        this.x = x + 10;
        this.y = y + 10;
    }
    abstract void scale(int x, int y, int k);
    abstract void squareShapes(int x, int y);*/
    
}
